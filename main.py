import discord
import os
import sys
from sgxbot import sgxlogging as log
from sgxbot import management
from sgxbot import bot


def censor(text):
    val = text[:3] + ('*' * (len(text) - 3)) if len(text) > 3 \
        else '*' * len(text)
    return val


def main(argv):
    token = ''

    if len(argv) > 1:
        token = argv[1]
    elif os.path.isfile('token.private'):
        with open('token.private', 'r') as tokenfile:
            token = tokenfile.readline().strip()
    elif os.environ['TOKEN']:
        token = os.environ['TOKEN']
    else:
        log.die('Failed to read token! Create a token.private file or specify\
 one as the first argument.')

    log.info('Attempting login with token: {}'.format(censor(token)))
    try:
        bot.run_forever(token)
    except discord.ConnectionClosed as e:
        log.error(e.reason)
        log.die('Lost connection. Details are listed above.')


if __name__ == '__main__':
    main(sys.argv)
