# -*- coding: utf-8 -*-

import asyncio
import discord
import hashlib
import os
import random
import re
import urllib3
from sgxbot import algorithms as alg
from sgxbot import bot
from sgxbot import sgxlogging as log
from sgxbot import management as mgr
from sgxbot import version


persistent = {
    'ERRORS': 0,
}


boxvisuals = []
user_agent = {'user-agent': 'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0'}
http = urllib3.PoolManager(10, headers=user_agent)


def download_image(url, path):
    request = http.request('GET', url)
    if request.status is 200:
        with open(path, 'wb+') as f:
            f.write(request.data)


def get_remember():
    with open('remember.txt', 'r') as remembered:
        return '\n'.join(remembered)


def set_remember(msg):
    with open('remember.txt', 'w') as remembered:
        remembered.write(msg)


async def handle_image(client, message):
    if not alg.WORKING['IMG']:
        if (message.channel.topic is not None and '[sgxbot autorespond]' in message.channel.topic):
            for attachment in message.attachments:
                if (attachment['url'].endswith('.png') or attachment['url'].endswith('.jpg')):
                    boxvisuals.append(message.attachments[0]['url'])
            if len(boxvisuals) >= 9:
                try:
                    count = 0
                    for addr in boxvisuals:
                        print('downloading image: ' + addr)
                        download_image(addr, 'img_{}'.format(count))
                        count += 1
                    boxvisuals.clear()
                    alg.make_box_visuals()
                    await client.send_file(message.channel, 'boxvisuals.jpg', content='Check Out My New Remix')
                except Exception as e:
                    raise e

async def handle_random(client, message: discord.Message):
    if message.channel.topic is not None and '[sgxbot autorespond]' in message.channel.topic:
        previous = []
        async for logged in client.logs_from(message.channel, limit=4):
            previous.append(logged)
        target = alg.alphanumeric(previous[0].content)
        original = previous[0].content
        if target.strip() != '':
            matches = 0
            for repeat in previous:
                if target == alg.alphanumeric(repeat.content):
                    matches += 1
                else:
                    break
            if matches == 4:
                await client.send_message(message.channel, original)
                return

        if random.randint(0, 100) <= 2:
            msgs = mgr.get(message.server, 'messages')
            if mgr.get(message.server, 'pmessages') is not None and \
               len(mgr.get(message.server, 'pmessages')) > 0:
                msgs = msgs + mgr.get(message.server, 'pmessages')

            choice = '(no messages available)'

            choice = random.choice(alg.get_appropriate_responses(message.content, msgs)) or '(no choices available)'

            await client.send_message(message.channel, choice)
            return

        if random.randint(0, 100) <= 3:
            msgs = mgr.get(message.server, 'messages')
            if mgr.get(message.server, 'pmessages') is not None and \
               len(mgr.get(message.server, 'pmessages')) > 0:
                msgs = msgs + mgr.get(message.server, 'pmessages')

            choice = '(no messages available)'

            choice = random.choice(alg.get_appropriate_responses(message.content, msgs)) or '(no choices available)'

            await client.send_message(message.channel, '[{0.author.name}] "{0.content}" - Sparta {1} Remix'.format(message, choice))
            return


async def handle_mention(client, message):
    if message.author.id == '146778190176714753':
        await client.send_message(message.channel, 'lillie is 11')
        return
    msgs = mgr.get(message.server, 'messages')
    if mgr.get(message.server, 'pmessages') is not None and \
       len(mgr.get(message.server, 'pmessages')) > 0:
        msgs = msgs + mgr.get(message.server, 'pmessages')

    choice = '(no messages available)'

    if len(message.content.split(' ')) <= 1:
        choice = random.choice(msgs)
    else:
        choice = random.choice(alg.get_appropriate_responses(' '.join(message.content.split(' ')[1:]), msgs))

    await client.send_message(message.channel, message.author.mention + ' ' +
                              choice)


async def handle_command(client, message):
    if message.author.id == '146778190176714753':
        await client.send_message(message.channel, 'no')
        return
    cmdname = message.content.split(' ')[0][1:].lower()
    log.info('Command executed by {}#{}: {}'
             .format(message.author.name,
                     message.author.discriminator,
                     cmdname))
    if cmdname in commands:
        try:
            await commands[cmdname](client, message)
            persistent['ERRORS'] = 0
        except Exception as e:
            await client.send_message(message.channel,
                                      embed=generate_error_embed(e,
                                                                 cmdname))
            log.error('Caught {} when executing command!'.format(e))
            persistent['ERRORS'] += 1
            if persistent['ERRORS'] > 10:
                embed = discord.Embed()
                embed.title = 'More than 10 errors in a row!'
                embed.type = 'rich'
                embed.color = 0xFF1111
                embed.description = \
                    'Bot will be shut off to prevent further damage.'
                await client.send_message(message.channel, embed=embed)
                exit(-1)
    else:
        await invalid(client, message)


def generate_error_embed(reason, command):
    embed = discord.Embed()
    embed.title = 'Error executing {}!'.format(command)
    embed.type = 'rich'
    embed.color = 0xFF1111
    embed.description = 'Here\'s some information: {}'.format(reason)
    return embed


def get_reaction_amount(message, target):
    reactions = message.reactions
    for reaction in reactions:
        if reaction.emoji == target:
            return reaction.count
    return 0


async def invalid(client, message):
    log.warn('Unknown command, displaying help message.')
    await client.send_message(message.channel,
                              'Unknown command. Try {}help'.format(bot.PREFIX))


async def show_help(client, message):
    """Displays the bot's help message."""
    embed = discord.Embed()
    embed.title = 'SGXbot 2 Commands\n'
    embed.type = 'rich'
    embed.colour = 0x8CFFA8
    embed.description = 'All currently available commands.'
    embed.set_footer(text='uh oh sgxbot alert ({})'.format(version.VERSION))
    for (key, value) in commands.items():
        doc = value.__doc__ if value.__doc__ else '(No description available.)'
        embed.add_field(name=(bot.PREFIX + key), value=doc)
    await client.send_message(message.channel, embed=embed)


async def add_msg(client, message, ignore_first=True):
    """Usage: add [message]
Adds a message to this server's message list."""
    start = 1 if ignore_first else 0
    arg = ' '.join(message.content.split(' ')[start:]).strip()
    if arg.startswith('Added "'):
        await client.send_message(message.channel, 'fuck off')
    elif len(message.attachments) > 0 and arg == '' \
            and 'url' in message.attachments[0]:
        target = message.attachments[0]['url']
        success = mgr.append(message.server, 'messages', target)
        result = 'Added image! {}'.format(target) \
            if success else "Message already exists."
        await client.send_message(message.channel, result)
    elif len(arg) > 0:
        success = mgr.append(message.server, 'messages', arg)
        result = 'Added "{}"!'.format(arg) \
            if success else "Message already exists."
        await client.send_message(message.channel, result)


async def perm_add_msg(client, message, ignore_first=True):
    """Usage (if you are notjoe_): xadd [message]
Permanently adds a message to this server's message list."""
    if message.author.id == '124257651747061760':
        start = 1 if ignore_first else 0
        arg = ' '.join(message.content.split(' ')[start:]).strip()
        if len(message.attachments) > 0 and arg == '' \
                and 'url' in message.attachments[0]:
            target = message.attachments[0]['url']
            success = mgr.append(message.server, 'pmessages', target)
            result = 'Permanently added image! {}'.format(target) \
                if success else "Message already exists."
            await client.send_message(message.channel, result)
        elif len(arg) > 0:
            success = mgr.append(message.server, 'pmessages', arg)
            result = 'Permanently added "{}"!'.format(arg) \
                if success else "Message already exists."
            await client.send_message(message.channel, result)
    else:
        await client.send_message(message.channel, "You can't use this command!")


async def add_previous(client, message):
    """Usage: a
Adds the previous message to this server's message list."""
    i = 0
    async for msg in client.logs_from(message.channel, limit=2):
        if i != 1:
            i = 1
        else:
            await add_msg(client, msg, ignore_first=False)


async def get_msg(client, message):
    """Usage: msg
Gets a random message from this server's message list."""
    msgs = mgr.get(message.server, 'messages')
    if mgr.get(message.server, 'pmessages') != None and \
       len(mgr.get(message.server, 'pmessages')) > 0:
        msgs = msgs + mgr.get(message.server, 'pmessages')

    await client.send_message(message.channel,
                              random.choice(msgs))


async def remove_msg(client, message):
    """Usage: del [exact message text]
Removes a specified message from this server's message list."""
    target = ' '.join(message.content.split(' ')[1:]).strip()
    success = mgr.erase_from_list(message.server, 'messages', target)
    result = "Removed the specified message from the list!" \
        if success else "Message not found in list."
    await client.send_message(message.channel, result)


async def show_msgs(client, message):
    """Usage: show
Shows the number of messages in this server's message list."""
    msgs = mgr.get(message.server, 'messages')
    if msgs is None or len(msgs) <= 0:
        await client.send_message(message.channel,
                                  "**0** messages on this server. (**0 B**)")
        return
    await client.send_message(message.channel,
                              "**{}** messages on this server. (**{}**)"
                              .format(len(msgs), mgr.get_filesize(message.server)))
    return


async def poll(client, message):
    """Usage: poll [question]
Host a 1-minute poll where users can vote yes/no/both via reactions."""
    arg = ' '.join(message.content.split(' ')[1:]).strip()
    if len(arg) > 0:
        embed = discord.Embed()
        embed.title = arg
        embed.type = 'rich'
        embed.colour = 0x32B6Ef
        embed.description = \
            'This poll ends 1 minute after this message was sent.'
        url = message.author.avatar_url or Embed.Empty
        embed.set_author(name=message.author.display_name,
                         icon_url=url)

        sender = message.author
        sent = await client.send_message(message.channel, embed=embed)
        await client.add_reaction(sent, '👍')   # Thumbs up emoji
        await client.add_reaction(sent, '👎')   # Thumbs down emoji
        await asyncio.sleep(30)

        try:
            dummy = await client.get_message(sent.channel, sent.id)
        except Exception as e:
            return

        reminder = await client.send_message(message.channel,
                                             message.author.display_name
                                             + "'s poll ends in 30 seconds!")
        await asyncio.sleep(30)

        try:
            sent = await client.get_message(sent.channel, sent.id)
            pos = get_reaction_amount(sent, '👍') - 1
            neg = get_reaction_amount(sent, '👎') - 1
            await client.delete_message(sent)
        except Exception as e:
            return
        else:
            resultembed = discord.Embed()
            resultembed.title = 'Results for poll: ' + arg
            resultembed.type = 'rich'
            resultembed.colour = 0x32B6Ef
            url = message.author.avatar_url or Embed.Empty
            resultembed.set_author(name=message.author.display_name,
                                   icon_url=url)
            resultembed.add_field(name='Voted Yes', value=pos)
            resultembed.add_field(name='Voted No', value=neg)

            try:
                await client.delete_message(reminder)
            except:
                pass
            await client.send_message(message.channel, message.author.mention,
                                      embed=resultembed)


async def repeat(client, message):
    """Usage: repeat [message]
Repeats a given message."""
    msg = ' '.join(message.content.split(' ')[1:]).strip()
    if len(msg) > 0:
        await client.send_message(message.channel, msg)


async def remember_msg(client, message):
    """Usage (view current message): remember
Usage (overwrite current message): remember [message]
Sets a message that is accessible from all servers."""
    msg = ' '.join(message.content.split(' ')[1:]).strip()
    if len(msg) > 0:
        set_remember(msg)
        await client.send_message(message.channel,
                                  'Saved message: "{}"!'.format(msg))
    else:
        await client.send_message(message.channel,
                                  get_remember())


async def get_id(client, message):
    print(message.author.id)


async def question(client, message):
    """Usage: question [question]
Responds to a yes/no(/maybe) question.
WIP: Should always give the same response to a question, but changes after reboots.
"""
    msg = ' '.join(message.content.split(' ')[1:]).strip()
    if len(msg) > 0:
        cleaned = re.sub(r'\W+', '', msg.replace(' ', '').lower())
        seed = abs(hash(cleaned)) % (10**8)
        customrand = random.Random()
        customrand.seed(seed)
        response = customrand.choice([
            'Yes',
            'No',
            'Maybe',
            'Probably not',
            'Probably',
            'Sometimes'
        ])

        await client.send_message(message.channel, "{}, **{}**.".format(message.author.mention, response))


async def add_stultus(client, message):
    """Usage (get in/out of line to fuck the furry): fuckstultus
Usage (see how many people want to fuck the furry): fuckstultus stats
Get in line.
"""
    msg = message.content.lower().split(' ')[1:]
    data = mgr.get(message.server, 'gay')
    if data is None:
        mgr.set(message.server, 'gay', [])
    if msg is not None and len(msg) > 0 and msg[0] == 'stats':
        value = len(mgr.get(message.server, 'gay'))
        suffix = 'person wants to fuck Stultus' if value == 1 else ""
        await client.send_message(message.channel, "{}, **{}** people want to fuck Stultus.".format(message.author.mention, len(mgr.get(message.server, 'gay'))))
    else:
        if message.author.id in data:
            mgr.erase_from_list(message.server, 'gay', message.author.id)
            await client.send_message(message.channel, "{}, you no longer want to fuck Stultus.".format(message.author.mention))
        else:
            mgr.append(message.server, 'gay', message.author.id)
            await client.send_message(message.channel, "{}, you now want to fuck Stultus!".format(message.author.mention))


def count(string, opportunities, wordsOnly=False):
    matches = 0
    for value in opportunities:
        if wordsOnly:
            if string in value.lower().split(' '):
                matches += 1
        else:
            if string in value.lower():
                matches += 1
    return matches


async def count_msgs(client, message):
    """Usage: count [phrase]
Counts the number of messages that contain a phrase.
"""
    msg = message.content.strip().lower().split(' ')[1:]
    data = mgr.get(message.server, 'messages')
    if msg is not None and len(msg) > 0:
        search = ' '.join(msg)
        matchedWords = count(search, data, wordsOnly=True)
        matched = count(search, data)

        await client.send_message(message.channel, "**{}** messages contain **{}** somewhere within them.\n**{}** messages contain **{}** as a word."\
            .format(matched, search, matchedWords, search))


async def jario(client, message):
    """Usage: jario [image]
Jario Screaming Remix
"""
    if len(message.attachments) > 0:
        if 'url' in message.attachments[0] and \
                (message.attachments[0]['url'].endswith('.png') or message.attachments[0]['url'].endswith('.jpg')):
            ending = message.attachments[0]['url'].split('.')[-1:]
            filename = '{}.{}'.format(message.author.id, ending[0])
            download_image(message.attachments[0]['url'], filename)
            alg.make_jario(filename)
            await client.send_file(message.channel, 'jario' + filename, content='Jared')
            os.remove('jario' + filename)


async def spartadubs(client, message):
    """Usage: jario [image]
Jario Screaming Remix
"""
    if len(message.attachments) > 0:
        if 'url' in message.attachments[0] and \
                (message.attachments[0]['url'].endswith('.png') or message.attachments[0]['url'].endswith('.jpg')):
            ending = message.attachments[0]['url'].split('.')[-1:]
            filename = '{}.{}'.format(message.author.id, ending[0])
            download_image(message.attachments[0]['url'], filename)
            alg.make_dubs(filename)
            await client.send_file(message.channel, 'sd' + filename, content='This is for you Jario. Thanks a lot to make for SpartaDubs! I hope you like it :D.')
            os.remove('sd' + filename)


async def reboot(client, message):
    """Usage: reboot
Restarts the bot. May take a few minutes."""
    await client.send_message(message.channel,
                              "**Restarting.**")
    exit(50)


commands = {
    'help': show_help,
    'add': add_msg,
    'xadd': perm_add_msg,
    'a': add_previous,
    'msg': get_msg,
    'del': remove_msg,
    'show': show_msgs,
    'count': count_msgs,
    'jario': jario,
    'spartadubs': spartadubs,
    'poll': poll,
    # 'id': get_id,
    'fuckstultus': add_stultus,
    'r': repeat,
    'remember': remember_msg,
    'question': question,
    'reboot': reboot
}
