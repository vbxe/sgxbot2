import datetime
import sys

INFO_PREFIX = 'I.'
WARN_PREFIX = 'W!'
ERROR_PREFIX = 'E!'
CRITICAL_PREFIX = 'C!'


def log(message, prefix=INFO_PREFIX, destination=sys.stdout):
    destination.write('[{} {}] {}\n'.format(datetime.datetime.now(),
                                            prefix, message))


def info(message):
    log(message)


def warn(message):
    log(message, prefix=WARN_PREFIX)


def error(message):
    log(message, prefix=ERROR_PREFIX, destination=sys.stderr)


def die(reason, exitcode=-1):
    log(reason, prefix=CRITICAL_PREFIX, destination=sys.stderr)
    exit(exitcode)
