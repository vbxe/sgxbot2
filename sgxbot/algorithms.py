import os
import random
import re
import PIL.Image


YN_QUESTION_WORDS = [
    'is', 'am', 'are', 'does', 'have', 'can', 'was', 'were', 'should', 'will', 'do', 'would'
]


POSITIONS = [
    (0,     0,      320,    180),
    (320,   0,      640,    180),
    (640,   0,      960,    180),
    (960,   0,      1280,   180),
    (0,     540,    320,    720),
    (320,   540,    640,    720),
    (640,   540,    960,    720),
    (960,   540,    1280,   720)
]

JARIO_SIZE = (528, 528)
DUBS_SIZE = (200, 290)
DUBS_OVERLAY = (200, 191)

WORKING = {
    'IMG': False
}


def alphanumeric(text):
    return re.sub(r'[^a-zA-Z\d\s]', '', text)


def get_appropriate_responses(message, choices):
    tokens = alphanumeric(message.lower()).split(' ')
    matches = []
    print(tokens)
    if tokens[0] in YN_QUESTION_WORDS:
        for choice in choices:
            if alphanumeric(choice.lower()).split(' ')[0] == 'yes' or \
               alphanumeric(choice.lower()).split(' ')[0] == 'no':
                matches.append(choice)
    else:
        for choice in choices:
            if len(set(tokens).intersection(set(alphanumeric(choice.lower()).split(' ')))) > len(tokens) / 3:
                matches.append(choice)

    if len(matches) < 2:
        matches = choices

    return matches


def make_box_visuals():
    if not WORKING['IMG']:
        WORKING['IMG'] = True
        canvas = PIL.Image.new('RGBA', (1280, 720), 'black')
        files = []
        for filename in os.listdir('.'):
            if filename.startswith('img_'):
                files.append(filename)
        if len(files) < 9:
            return
        files = sorted(files)
        canvas.paste(PIL.Image.open(files[0]).convert('RGBA').resize((1280, 720), PIL.Image.ANTIALIAS), (0, 0, 1280, 720))
        for i in range(1, 9):
            image = PIL.Image.open(files[i]).convert('RGBA').resize((320, 180), PIL.Image.ANTIALIAS)
            if random.randint(0, 4) == 2:
                image = image.transpose(PIL.Image.FLIP_LEFT_RIGHT)
            elif random.randint(0, 4) == 3:
                image = image.transpose(PIL.Image.FLIP_TOP_BOTTOM)
            canvas.paste(image, POSITIONS[i - 1])
        canvas.save('boxvisuals.jpg', 'JPEG')

        for filename in files:
            os.remove(filename)
        WORKING['IMG'] = False


def make_jario(filename):
    if not WORKING['IMG']:
        WORKING['IMG'] = True
        base = PIL.Image.open(filename, 'r').convert('RGBA').resize(JARIO_SIZE, PIL.Image.ANTIALIAS)
        overlay = PIL.Image.open('jariotemplate.png', 'r')
        base.paste(overlay, (0, 0), overlay)
        base.save('jario{}'.format(filename))
        os.remove(filename)
        WORKING['IMG'] = False

def make_dubs(filename):
    if not WORKING['IMG']:
        WORKING['IMG'] = True
        base = PIL.Image.new('RGBA', DUBS_SIZE)
        base.paste(PIL.Image.open(filename, 'r').convert('RGBA').resize(DUBS_OVERLAY, PIL.Image.ANTIALIAS), (0, 0, 200, 191))
        overlay = PIL.Image.open('dubs_template.png', 'r').convert('RGBA')
        base.paste(overlay, (0, 0, 200, 290), overlay)
        base.save('sd{}'.format(filename))
        os.remove(filename)
        WORKING['IMG'] = False


if __name__ == '__main__':
    make_jario('maxresdefault.jpg')
