import discord
import os
import json


def get_filename(server):
    return '{}.json'.format(server.id)


def sizeof_fmt(num, suffix='B'):
    for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)


def get_filesize(server):
    fname = get_filename(server)
    if os.path.isfile(fname):
        return sizeof_fmt(os.path.getsize(fname))
    else:
        return '0 B'


def get(server, key):
    fname = get_filename(server)
    if os.path.isfile(fname):
        with open(fname, 'r') as serverdata:
            jobj = json.loads(serverdata.read())
            try:
                return jobj[key]
            except KeyError as e:
                return None
    else:
        return None


def set(server, key, value):
    fname = get_filename(server)
    jobj = {}
    if os.path.isfile(fname):
        with open(fname, 'r') as serverdata:
            jobj = json.loads(serverdata.read())
    jobj[key] = value
    with open(fname, 'w') as serverdata:
        serverdata.write(json.dumps(jobj))


def erase_from_list(server, key, value):
    fname = get_filename(server)
    jobj = {}
    if os.path.isfile(fname):
        with open(fname, 'r') as serverdata:
            jobj = json.loads(serverdata.read())
    if value in jobj[key]:
        jobj[key].remove(value)
        with open(fname, 'w') as serverdata:
            serverdata.write(json.dumps(jobj))
            return True
    return False


def append(server, key, value):
    fname = get_filename(server)
    jobj = {}
    if os.path.isfile(fname):
        with open(fname, 'r') as serverdata:
            jobj = json.loads(serverdata.read())
    else:
        jobj[key] = []
    if key not in jobj:
        jobj[key] = []
    elif value in jobj[key]:
        return False
    jobj[key].append(value)
    with open(fname, 'w') as serverdata:
        serverdata.write(json.dumps(jobj))
        return True
