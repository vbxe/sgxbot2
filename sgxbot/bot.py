import asyncio
import discord
from sgxbot import sgxlogging as log
from sgxbot import commands

CLIENT = discord.Client()
PREFIX = '%'


def run_forever(token):
    CLIENT.run(token)


@CLIENT.event
async def on_ready():
    log.info('Successfully logged in as {} : {}'.format(CLIENT.user.name,
                                                        CLIENT.user.id))
    await CLIENT.change_status(discord.Game(name='%help'))


@CLIENT.event
async def on_message(message):
    if message.content.startswith(PREFIX) and \
            not message.author == CLIENT.user:
        await commands.handle_command(CLIENT, message)
    elif CLIENT.user in message.mentions and \
            not message.author == CLIENT.user:
        await commands.handle_mention(CLIENT, message)
    elif len(message.attachments) > 0 and not message.author == CLIENT.user:
        await commands.handle_image(CLIENT, message)
    elif CLIENT.user not in message.mentions and \
            not message.author == CLIENT.user:
        await commands.handle_random(CLIENT, message)

